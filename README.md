[![build status](https://gitlab.com/zzh8829/hello/badges/master/build.svg)](https://gitlab.com/zzh8829/hello/commits/master)
[![coverage report](https://gitlab.com/zzh8829/hello/badges/master/coverage.svg)](https://gitlab.com/zzh8829/hello/commits/master)

# Hello
This repo is for testing my gitlab and kubernetes setup

## Requirements
docker, gitlab, node and a kubernetes cluster with ingress-controller

## Local Development 
You can use node.js directly or use docker container

#### Node.js
```
node index.js
```
#### Docker 
```
docker build -t hello .
docker run -d -p 8000:8000 hello
```

After either of them, you should be able to test with
```
curl localhost:8000
```

If you are using docker, don't forget to clean up with
```
docker stop $(docker ps -a | grep hello | awk '{print $1}')
docker rm $(docker ps -a | grep hello | awk '{print $1}')
```

### Create Kubernetes Deploy
```
# this will create deployment, service and ingress for hello
kubectl create -f kube
```
If you don't know what these are read the kubernetes doc first.
Note the pod will fail since docker image doesn't exist in registry yet

### Gitlab CI + Deploy
You need to set up a pipeline variables to access kubernetes cluster
```
# encode your kubectl config file in base64
uuencode -m config ~/kube/config ~/kube/config 
pbcopy < config
```
set KUBE_CONFIG_BASE64 = config string in pipline variable settings

Now just push master !!! Gitlab CI will build and push docker image and patch kubernetes deployment config to roll out an auto update

EVERYTHING IS SO MAGICAL 
```
# this actually works
curl hello.kube.zihao.ca
```
